﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSysHelper
{
    public static class ErrorChecking
    {
        public static bool IsEmpty(TextBox tb)
        {
            if (String.IsNullOrWhiteSpace(tb.Text))
            {
                throw new Exception(tb.Tag.ToString());
            }

            return true;
        }

        public static bool IsEmpty(ComboBox cb)
        {
            if (String.IsNullOrWhiteSpace(cb.Text))
            {
                throw new Exception(cb.Tag.ToString());
            }

            return true;
        }

        public static bool ArePasswordsEqual(TextBox firstPass, TextBox secondPass)
        {
            if (firstPass.Text.Equals(secondPass.Text))
                return true;

            MessageBox.Show("Passwords are not equal.  Please try again", "Passwords not equal", MessageBoxButtons.OK, MessageBoxIcon.Error);
            firstPass.Clear();
            secondPass.Clear();
            firstPass.Focus();
            return false;
        }

        public static bool AreValidPasswords(TextBox firstPass, TextBox secondPass)
        {
            if ((firstPass.Text.Length > 5) && (secondPass.Text.Length > 5))
                return true;

            MessageBox.Show("Passwords are not valid. Length must be at least 6 characters. Please try again", "Passwords not valid", MessageBoxButtons.OK, MessageBoxIcon.Error);
            firstPass.Clear();
            secondPass.Clear();
            firstPass.Focus();
            return false;
        }
    }
}
