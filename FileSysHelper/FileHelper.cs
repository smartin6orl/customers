﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.Configuration;

namespace FileSysHelper
{
    public class FileHelper
    {
        private static readonly Object locker = new Object();
        public static readonly string APPLICATION_DIRECTORY = String.Format("{0}\\SecuritySuite\\", Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles));
        //public static readonly string APPLICATION_DIRECTORY = @"C:\Users\dkbik\Desktop\SecuritySuite\SecuritySuite\bin\Debug\";
        public static readonly string APPLICATION_DATA_DIRECTORY = String.Format("{0}\\SecuritySuite\\", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));

        public static void SaveTextToDisk(string text, string path)
        {
            File.WriteAllText(path, text);
        }

        public static bool SaveBytesToDisk(byte[] data, string path)
        {
            if (data.Length == 0 || data == null)
                return true;

            try
            {
                File.WriteAllBytes(path, data);

                if (FileExists(path))
                {
                    return new FileInfo(path).Length == data.Length; // If true, then the file exists and is the same size as the byte array
                }
            }

            catch
            {
                
            }

            return false;
        }

        public static byte[] ReadBytesFile(string path)
        {
            return File.ReadAllBytes(path);
        }

        public static string ReadTextFile(string path)
        {
            return File.ReadAllText(path);
        }

        public static string[] ReadAllLines(string path)
        {
            return File.ReadAllLines(path);
        }

        public static void WriteAllText(string path, string text)
        {
            File.WriteAllText(path, text);
        }

        public static string GetSaveFilePath(byte type)
        {
            string path = String.Empty;
            SaveFileDialog sfd = new SaveFileDialog();

            switch (type)
            {
                case 0:
                    sfd.Filter = "Security Suite file (*.peot)|*.peot";
                    break;

                case 1:
                    sfd.Filter = "Security Suite file (*.peob)|*.peob";
                    break;

                case 2:
                    sfd.Filter = "Password Storage file (*.peop)|*.peop";
                    break;

                default:
                    return null;
            }

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                path = sfd.FileName;
            }

            return path;
        }

        public static string GetOpenFilePath()
        {
            string path = String.Empty;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Security Suite Text file (*.peot)|*.peot|Security Suite Binary file (*.peob)|*.peob";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                path = ofd.FileName;
            }

            return path;
        }

        public static void AddConfigSetting(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {

                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
        }

        public static string ReadConfigValue(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string value = appSettings[key];
                string path = (value != null) ? appSettings[key] : String.Empty;
                return path;
            }
            catch (ConfigurationErrorsException)
            {
                return String.Empty;
            }
        }

        public static Dictionary<string, string> ReadAllSettings()
        {
            Dictionary<string, string> settings = new Dictionary<string, string>();

            try
            {
                var appSettings = ConfigurationManager.AppSettings;

                if (appSettings.Count == 0)
                {
                    Console.WriteLine("AppSettings is empty.");
                }
                else
                {
                    foreach (string key in appSettings.AllKeys)
                    {
                        settings.Add(key, appSettings[key]);
                    }
                }
            }
            catch (ConfigurationErrorsException)
            {
                settings.Clear();
            }

            return settings;
        }

        public static bool FileExists(string path)
        {
            return File.Exists(path);
        }

        public static void PathExists(string path)
        {
            Directory.CreateDirectory(path);
        }

        public static byte[] GetByteArrayFromObject(Object obj)
        {
            MemoryStream fs = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();

            try
            {
                lock (locker)
                {
                    formatter.Serialize(fs, obj);
                }
                return fs.ToArray();
            }
            catch(Exception ex)
            {
                return null;
            }
            finally
            {
                fs.Close();
            }
        }

        public static void CreateDataPath()
        {
            PathExists(APPLICATION_DATA_DIRECTORY);
        }
    }
}