﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Security;
using System.Security.Cryptography;
using System.Threading;
using FileSysHelper;

namespace Encryption
{
    public class Encryptor
    {
        /// <summary>
        /// Encrypts plain text
        /// </summary>
        /// <param name="plain">The text to be encrypted</param>
        /// <param name="key">The key used to encrypt the text</param>
        /// <returns>String of encrypted text</returns>
        public static string EncryptTextAsText(string plain, string key)
        {
            if (plain == null || plain.Length == 0) return null;

            byte[] data = Encoding.UTF8.GetBytes(plain);
            byte[] encrypted = PerformEncryption(data, key);

            return Convert.ToBase64String(encrypted);
        }

        /// <summary>
        /// Encrypts plain text
        /// </summary>
        /// <param name="plain">The text to be encrypted</param>
        /// <param name="key">The key used to encrypt the text</param>
        /// <returns>Byte array of encrypted text</returns>
        public static byte[] EncryptTextAsBytes(string plain, string key)
        {
            if (plain == null || plain.Length == 0) return null;

            byte[] data = Encoding.UTF8.GetBytes(plain);
            return PerformEncryption(data, key);
        }

        /// <summary>
        /// Encrypts a byte array
        /// </summary>
        /// <param name="info">The byte array to encrypt</param>
        /// <param name="key">The key used to encrypt the data</param>
        /// <returns>String of encrypted text</returns>
        public static string EncryptBytesAsText(byte[] info, string key)
        {
            if (info == null || info.Length == 0) return null;

            byte[] encrypted = PerformEncryption(info, key);
            return Convert.ToBase64String(encrypted);
        }

        /// <summary>
        /// Encrypts a byte array
        /// </summary>
        /// <param name="info">The byte array to encrypt</param>
        /// <param name="key">The key used to encrypt the data</param>
        /// <returns>Byte array of encrypted bytes</returns>
        public static byte[] EncryptBytesAsBytes(byte[] info, string key)
        {
            if (info == null || info.Length == 0) return null;

            return PerformEncryption(info, key);
        }

        

        /// <summary>
        /// Decrypts plain text
        /// </summary>
        /// <param name="cipher">The text to be decrypted</param>
        /// <param name="key">The key used to encrypt the string</param>
        /// <param name="sleep">The length of time in milliseconds you want the method to pause for after a wrong key is entered</param>
        /// <returns>String of decrypted text</returns>
        public static string DecryptTextAsText(string cipher, string key, int sleep)
        {
            if (cipher == null || cipher.Length == 0) return null;

            byte[] data = Convert.FromBase64String(cipher);
            
            try
            {
                byte[] decrypted = PerformDecryption(data, key);

                string info = Convert.ToBase64String(decrypted);
                var v = Convert.FromBase64String(info);
                return Encoding.UTF8.GetString(v);
            }

            catch(Exception ex)
            {
                Thread.Sleep(sleep);
                throw new Exception(ex.Message); // Let the exception bubble up to the top
            }
        }

        /// <summary>
        /// Decrypts plain text
        /// </summary>
        /// <param name="cipher">The text to be decrypted</param>
        /// <param name="key">The key used to encrypt the string</param>
        /// <param name="sleep">The length of time in milliseconds you want the method to pause for after a wrong key is entered</param>
        /// <returns>Byte array of decrypted bytes</returns>
        public static byte[] DecryptTextAsBytes(string cipher, string key, int sleep)
        {
            if (cipher == null || cipher.Length == 0) return null;

            byte[] data = Convert.FromBase64String(cipher);

            try
            {
                return PerformDecryption(data, key);
            }

            catch
            {
                Thread.Sleep(sleep);
                throw new IOException(); // Let the exception bubble up to the top
            }
        }

        /// <summary>
        /// Decrypts byte array
        /// </summary>
        /// <param name="cipher">The byte array to be decrypted</param>
        /// <param name="key">The key used to encrypt the byte array</param>
        /// <param name="sleep">The length of time in milliseconds you want the method to pause for after a wrong key is entered</param>
        /// <returns>String of decrypted bytesf</returns>
        public static string DecryptBytesAsText(byte[] cipher, string key, int sleep)
        {
            if (cipher == null || cipher.Length == 0) return null;
            
            try
            {
                byte[] decrypted = PerformDecryption(cipher, key);

                string info = Convert.ToBase64String(decrypted);
                var v = Convert.FromBase64String(info);
                return Encoding.UTF8.GetString(v);
            }

            catch
            {
                Thread.Sleep(sleep);
                throw new IOException(); // Let the exception bubble up to the top
            }
        }

        /// <summary>
        /// Decrypts byte array
        /// </summary>
        /// <param name="cipher">The byte array to be decrypted</param>
        /// <param name="key">The key used to encrypt the byte array</param>
        /// <param name="sleep">The length of time in milliseconds you want the method to pause for after a wrong key is entered</param>
        /// <returns>Byte array of decrypted bytes</returns>
        public static byte[] DecryptBytesAsBytes(byte[] cipher, string key, int sleep)
        {
            if (cipher == null || cipher.Length == 0) return null;

            try
            {
                return PerformDecryption(cipher, key);
            }

            catch
            {
                Thread.Sleep(sleep);
                throw new Exception();
            }
        }



        /// <summary>
        /// Uses SHA256 to return has of byte array
        /// </summary>
        /// <param name="objectAsBytes">The byte array to generate the hash of</param>
        /// <returns>SHA256 has of byte array</returns>
        public static string ComputeSHA256Hash(byte[] objectAsBytes)
        {
            SHA256 sha = new SHA256CryptoServiceProvider();

            try
            {
                byte[] result = sha.ComputeHash(objectAsBytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < result.Length; i++)
                {
                    sb.Append(result[i].ToString("X2"));
                }

                return sb.ToString();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Uses SHA256 to return has of byte array
        /// </summary>
        /// <param name="txt">The string to generate the hash of</param>
        /// <returns>SHA256 has of byte array</returns>
        public static string ComputeSHA256Hash(string txt)
        {
            byte[] bts = FileHelper.GetByteArrayFromObject(txt);
            return ComputeSHA256Hash(bts);
        }



        private static byte[] PerformEncryption(byte[] data, string key)
        {
            byte[] encrypted;

            string saltKeyStr = GenerateSaltKey(key);
            byte[] saltKeyB = Convert.FromBase64String(saltKeyStr);
            byte[] salt = new byte[16];
            byte[] keyBytes = new byte[32];
            Buffer.BlockCopy(saltKeyB, 0, salt, 0, 16);
            Buffer.BlockCopy(saltKeyB, 16, keyBytes, 0, 32);
            saltKeyStr = null;
            saltKeyB = null;

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged aes256 = new RijndaelManaged())
                {
                    aes256.KeySize = 256;
                    aes256.BlockSize = 128;
                    aes256.GenerateIV();
                    aes256.Padding = PaddingMode.PKCS7;
                    aes256.Mode = CipherMode.CBC;
                    aes256.Key = keyBytes;
                    keyBytes = null;

                    using (CryptoStream cs = new CryptoStream(ms, aes256.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        ms.Write(aes256.IV, 0, aes256.IV.Length);
                        ms.Write(salt, 0, 16);
                        cs.Write(data, 0, data.Length);
                    }
                }
                encrypted = ms.ToArray();
            }

            return encrypted;
        }

        private static byte[] PerformDecryption(byte[] data, string key)
        {
            byte[] decrypted;

            try
            {
                using (MemoryStream ms = new MemoryStream(data))
                {
                    using (RijndaelManaged aes256 = new RijndaelManaged())
                    {
                        byte[] iv = new byte[16];
                        byte[] salt = new byte[16];
                        ms.Read(iv, 0, 16);
                        ms.Read(salt, 0, 16);

                        aes256.KeySize = 256;
                        aes256.BlockSize = 128;
                        aes256.IV = iv;
                        aes256.Padding = PaddingMode.PKCS7;
                        aes256.Mode = CipherMode.CBC;
                        aes256.Key = GenerateKey(key, salt);

                        using (var cs = new CryptoStream(ms, aes256.CreateDecryptor(), CryptoStreamMode.Read))
                        {
                            byte[] temp = new byte[ms.Length - 16 - 16 + 1];
                            decrypted = new byte[cs.Read(temp, 0, temp.Length)];
                            Buffer.BlockCopy(temp, 0, decrypted, 0, decrypted.Length);
                        }
                    }
                }
            }

            catch
            {
                throw new Exception();
            }

            return decrypted;
        }

        private static string GenerateSaltKey(string key)
        {
            Rfc2898DeriveBytes rfc2898db = new Rfc2898DeriveBytes(key, 16, 10000);

            byte[] data = new byte[48];
            Buffer.BlockCopy(rfc2898db.Salt, 0, data, 0, 16);
            Buffer.BlockCopy(rfc2898db.GetBytes(32), 0, data, 16, 32);
            return Convert.ToBase64String(data);
        }

        private static byte[] GenerateKey(string key, byte[] salt)
        {
            Rfc2898DeriveBytes rfc2898db = new Rfc2898DeriveBytes(key, salt, 10000);
            return rfc2898db.GetBytes(32);
        }
    }
}
