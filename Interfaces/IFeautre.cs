﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IFeature
    {
        void Encrypt(string text, string key);
        void Decrypt(string text, string key);
        void ShowForm();
    }
}
