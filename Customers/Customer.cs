﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;

namespace Customers
{
    [Serializable]
    public class Customer
    {
        private int companynumber = int.MinValue;
        private string legalname = String.Empty;
        private string division = String.Empty;
        private string federalid = String.Empty;
        private string stateid = String.Empty;
        private string website = String.Empty;
        private string pc_name = String.Empty;
        private string pc_title = String.Empty;
        private string pc_phone = String.Empty;
        private string pc_email = String.Empty;
        private string bc_name = String.Empty;
        private string bc_title = String.Empty;
        private string bc_phone = String.Empty;
        private string bc_email = String.Empty;
        private string companykeycode = String.Empty;
        private DateTime dateinservice = DateTime.MinValue;
        private DateTime dateoutofservice = DateTime.MinValue;
        private int id_address_m = int.MinValue;
        private int id_address_b = int.MinValue;
        private int trialperioddays = int.MinValue;

        [JsonProperty("CompanyNumber")]
        public int CompanyNumber { get => companynumber; set => companynumber = value; }
        [JsonProperty("LegalName")]
        public string LegalName { get => legalname; set => legalname = value; }
        [JsonProperty("Division")]
        public string Division { get => division; set => division = value; }
        [JsonProperty("FederalID")]
        public string FederalID { get => federalid; set => federalid = value; }
        [JsonProperty("StateID")]
        public string StateID { get => stateid; set => stateid = value; }
        [JsonProperty("WebSite")]
        public string WebSite { get => website; set => website = value; }
        [JsonProperty("PC_Name")]
        public string PC_Name { get => pc_name; set => pc_name = value; }
        [JsonProperty("PC_Title")]
        public string PC_Title { get => pc_title; set => pc_title = value; }
        [JsonProperty("PC_Phone")]
        public string PC_Phone { get => pc_phone; set => pc_phone = value; }
        [JsonProperty("PC_Email")]
        public string PC_Email { get => pc_email; set => pc_email = value; }
        [JsonProperty("BC_Name")]
        public string BC_Name { get => bc_name; set => bc_name = value; }
        [JsonProperty("BC_Title")]
        public string BC_Title { get => bc_title; set => bc_title = value; }
        [JsonProperty("BC_Phone")]
        public string BC_Phone { get => bc_phone; set => bc_phone = value; }
        [JsonProperty("BC_Email")]
        public string BC_Email { get => bc_email; set => bc_email = value; }
        [JsonProperty("CompanyKeyCode")]
        public string CompanyKeyCode { get => companykeycode; set => companykeycode = value; }
        [JsonProperty("DateInService")]
        public DateTime DateInService { get => dateinservice; set => dateinservice = value; }
        [JsonProperty("DateOutOfService")]
        public DateTime DateOutOfService { get => dateoutofservice; set => dateoutofservice = value; }
        [JsonProperty("ID_Address_M")]
        public int ID_Address_M { get => id_address_m; set => id_address_m = value; }
        [JsonProperty("ID_Address_B")]
        public int ID_Address_B { get => id_address_b; set => id_address_b = value; }
        [JsonProperty("TrialPeriodDays")]
        public int TrialPeriodDays { get => trialperioddays; set => trialperioddays = value; }

        public static Customer GetCustomerFromID(int id)
        {
            Customer result = (DBData.Customers.ContainsKey(id)) ?
                (from c in DBData.Customers
                 where c.Key == id
                 select c.Value).First() : new Customer();
            return result;
        }
    }
}
