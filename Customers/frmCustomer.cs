﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Interfaces;
using FileSysHelper;
using Encryption;

namespace Customers
{
    public partial class frmCustomer : Form
    {
        Customer currentCustomer = new Customer();
        Address mail = new Address();
        Address bill = new Address();
        bool inEdit = false;

        public frmCustomer()
        {
            InitializeComponent();
            this.Tag = new Customer();
        }
        public frmCustomer(Customer edit)
        {
            InitializeComponent();
            currentCustomer = edit;
            displayCustomer();
            inEdit = true;
        }
        private void btnGenerateKeyCode_Click(object sender, EventArgs e)
        {
            string txCompanyCode = (this.txtBoxCompanyNumber.Text != null) ? String.Format("{0}", this.txtBoxCompanyNumber.Text) : String.Empty;
            string txLegalName = (this.txtBoxLegalName.Text != null) ? String.Format("{0}", this.txtBoxLegalName.Text) : String.Empty;
            string txtDivision = (this.txtBoxDivision.Text != null) ? String.Format("{0}", this.txtBoxDivision.Text) : String.Empty;
            string txt = txCompanyCode + txLegalName + txtDivision;
            string hash = Encryptor.ComputeSHA256Hash(txt);
            this.txtBoxCompanyKeyCode.Text = hash;
        }
        private void displayCustomer()
        {
            txtBoxCompanyNumber.Text = currentCustomer.CompanyNumber.ToString();
            txtBoxLegalName.Text = currentCustomer.LegalName;
            txtBoxDivision.Text = currentCustomer.Division;
            txtBoxFederalID.Text = currentCustomer.FederalID;
            txtBoxStateID.Text = currentCustomer.StateID;
            txtBoxWebSite.Text = currentCustomer.WebSite;
            txtBoxPC_Name.Text = currentCustomer.PC_Name;
            txtBoxPC_Title.Text = currentCustomer.PC_Title;
            txtBoxPC_Email.Text = currentCustomer.PC_Email;
            txtBoxPC_Phone.Text = currentCustomer.PC_Phone;
            txtBoxBC_Name.Text = currentCustomer.BC_Name;
            txtBoxBC_Title.Text = currentCustomer.BC_Title;
            txtBoxBC_Email.Text = currentCustomer.BC_Email;
            txtBoxBC_Phone.Text = currentCustomer.BC_Phone;
            txtBoxMail_ID.Text = currentCustomer.ID_Address_M.ToString();
            txtBoxBill_ID.Text = currentCustomer.ID_Address_B.ToString();
            txtBoxAddressM.Text = Address.GetAddressFromID(currentCustomer.ID_Address_M).ToString();
            txtBoxAddressB.Text = Address.GetAddressFromID(currentCustomer.ID_Address_B).ToString();
            txtBoxDateInService.Text = (currentCustomer.DateInService != DateTime.MinValue) ?
                currentCustomer.DateInService.ToString() : String.Empty;
            txtBoxTrialDays.Text = currentCustomer.TrialPeriodDays.ToString();
            txtBoxDateOutOfService.Text = (currentCustomer.DateOutOfService != DateTime.MinValue) ?
                currentCustomer.DateOutOfService.ToString() : String.Empty;
            txtBoxCompanyKeyCode.Text = currentCustomer.CompanyKeyCode;
            if (txtBoxCompanyKeyCode.Text == String.Empty)
                txtBoxCompanyKeyCode.Enabled = false;
        }

        private void frmCustomer_Load(object sender, EventArgs e)
        {

        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            frmAddresses addr = new frmAddresses(Address.ADDR_TYPE_MAILING);
            addr.ShowDialog();
            int mid = Convert.ToInt32(addr.Tag.ToString());
            txtBoxMail_ID.Text = addr.Tag.ToString();
            txtBoxAddressM.Text = Address.GetAddressFromID(mid).ToString();
        }
        private void btnBill_Click(object sender, EventArgs e)
        {
            frmAddresses addr = new frmAddresses(Address.ADDR_TYPE_BILLING);
            addr.ShowDialog();
            int bid = Convert.ToInt32(addr.Tag.ToString());
            txtBoxBill_ID.Text = addr.Tag.ToString();
            txtBoxAddressB.Text = Address.GetAddressFromID(bid).ToString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int err = 0;
            if (editCustomer(err))
            {
                MessageBox.Show("Please fix all errors");
                return;
            }
            Customer cust = saveCustomer();
            this.Tag = DBData.PostCustomerToDB(cust, inEdit) ? cust : new Customer();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool editCustomer(int err)
        {
            if (txtBoxCompanyNumber.Text == String.Empty ||
                !Regex.IsMatch(txtBoxCompanyNumber.Text, @"^\d+$") ||
                txtBoxCompanyNumber.Text.Length != 4)
            {
                txtBoxCompanyNumber.BackColor = Color.Red;
                err++;
            }
            if (txtBoxLegalName.Text == String.Empty)
            {
                txtBoxLegalName.BackColor = Color.Red;
                err++;
            }
            if (txtBoxFederalID.Text == String.Empty)
            {
                txtBoxFederalID.BackColor = Color.Red;
                err++;
            }
            if (txtBoxPC_Name.Text == String.Empty)
            {
                txtBoxPC_Name.BackColor = Color.Red;
                err++;
            }
            if (txtBoxPC_Title.Text == String.Empty)
            {
                txtBoxPC_Title.BackColor = Color.Red;
                err++;
            }
            if (txtBoxPC_Phone.Text == String.Empty)
            {
                txtBoxPC_Phone.BackColor = Color.Red;
                err++;
            }
            if (txtBoxPC_Email.Text == String.Empty)
            {
                txtBoxPC_Email.BackColor = Color.Red;
                err++;
            }
            if (txtBoxBC_Name.Text == String.Empty)
            {
                txtBoxBC_Name.BackColor = Color.Red;
                err++;
            }
            if (txtBoxBC_Title.Text == String.Empty)
            {
                txtBoxBC_Title.BackColor = Color.Red;
                err++;
            }
            if (txtBoxBC_Phone.Text == String.Empty)
            {
                txtBoxBC_Phone.BackColor = Color.Red;
                err++;
            }
            if (txtBoxBC_Email.Text == String.Empty)
            {
                txtBoxBC_Email.BackColor = Color.Red;
                err++;
            }
            if (txtBoxAddressM.Text == String.Empty)
            {
                txtBoxAddressM.BackColor = Color.Red;
                err++;
            }
            if (txtBoxAddressB.Text == String.Empty)
            {
                txtBoxAddressB.BackColor = Color.Red;
                err++;
            }
            if (txtBoxDateInService.Text == String.Empty ||
                !Regex.IsMatch(txtBoxDateInService.Text, @"^\d{1,2}\/\d{1,2}\/\d{4}$"))
            {
                txtBoxDateInService.BackColor = Color.Red;
                err++;
            }
            if (txtBoxTrialDays.Text == String.Empty ||
                !Regex.IsMatch(txtBoxTrialDays.Text, @"^\d{1,2}$"))
            {
                txtBoxTrialDays.BackColor = Color.Red;
                err++;
            }

            if (err != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private Customer saveCustomer()
        {
            Customer cur = new Customer();
            cur.CompanyNumber = Convert.ToInt32(txtBoxCompanyNumber.Text);
            cur.LegalName = txtBoxLegalName.Text;
            cur.Division = txtBoxDivision.Text;
            cur.FederalID = txtBoxFederalID.Text;
            cur.StateID = txtBoxStateID.Text;
            cur.WebSite = txtBoxWebSite.Text;
            cur.PC_Name = txtBoxPC_Name.Text;
            cur.PC_Title = txtBoxPC_Title.Text;
            cur.PC_Email = txtBoxPC_Email.Text;
            cur.PC_Phone = txtBoxPC_Phone.Text;
            cur.BC_Name = txtBoxBC_Name.Text;
            cur.BC_Title = txtBoxBC_Title.Text;
            cur.BC_Email = txtBoxBC_Email.Text;
            cur.BC_Phone = txtBoxBC_Phone.Text;
            cur.ID_Address_M = Convert.ToInt32(txtBoxMail_ID.Text);
            cur.ID_Address_B = Convert.ToInt32(txtBoxBill_ID.Text);
            cur.CompanyKeyCode = txtBoxCompanyKeyCode.Text;
            cur.DateInService = (txtBoxDateInService.Text != String.Empty) ?
                Convert.ToDateTime(txtBoxDateInService.Text) : DateTime.MinValue;
            cur.DateOutOfService = (txtBoxDateOutOfService.Text != String.Empty) ?
                Convert.ToDateTime(txtBoxDateOutOfService.Text) :  DateTime.MinValue;
            cur.TrialPeriodDays = (txtBoxTrialDays.Text != String.Empty) ?
                Convert.ToInt32(txtBoxTrialDays.Text) : int.MinValue;

            return cur;
        }

        private void chkBoxUsePrimary_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBoxUsePrimary.Checked)
            {
                txtBoxBC_Name.Text = txtBoxPC_Name.Text;
                txtBoxBC_Title.Text = txtBoxPC_Title.Text;
                txtBoxBC_Phone.Text = txtBoxPC_Phone.Text;
                txtBoxBC_Email.Text = txtBoxPC_Email.Text;
            }
        }
    }
}
