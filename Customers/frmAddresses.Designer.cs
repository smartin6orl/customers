﻿namespace Customers
{
    partial class frmAddresses
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dvgAddresses = new System.Windows.Forms.DataGridView();
            this.btnNewAddress = new System.Windows.Forms.Button();
            this.txtBoxAddrFilter = new System.Windows.Forms.TextBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.lblAddressType = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dvgAddresses)).BeginInit();
            this.SuspendLayout();
            // 
            // dvgAddresses
            // 
            this.dvgAddresses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgAddresses.Location = new System.Drawing.Point(12, 114);
            this.dvgAddresses.Name = "dvgAddresses";
            this.dvgAddresses.Size = new System.Drawing.Size(1123, 449);
            this.dvgAddresses.TabIndex = 0;
            this.dvgAddresses.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dvgAddresses_RowHeaderMouseDoubleClick);
            // 
            // btnNewAddress
            // 
            this.btnNewAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewAddress.Location = new System.Drawing.Point(1013, 42);
            this.btnNewAddress.Name = "btnNewAddress";
            this.btnNewAddress.Size = new System.Drawing.Size(122, 32);
            this.btnNewAddress.TabIndex = 1;
            this.btnNewAddress.Text = "New Address";
            this.btnNewAddress.UseVisualStyleBackColor = true;
            this.btnNewAddress.Click += new System.EventHandler(this.btnNewAddress_Click);
            // 
            // txtBoxAddrFilter
            // 
            this.txtBoxAddrFilter.Location = new System.Drawing.Point(12, 79);
            this.txtBoxAddrFilter.Name = "txtBoxAddrFilter";
            this.txtBoxAddrFilter.Size = new System.Drawing.Size(425, 20);
            this.txtBoxAddrFilter.TabIndex = 2;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(471, 79);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(75, 23);
            this.btnFilter.TabIndex = 3;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // lblAddressType
            // 
            this.lblAddressType.AutoSize = true;
            this.lblAddressType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddressType.Location = new System.Drawing.Point(12, 25);
            this.lblAddressType.Name = "lblAddressType";
            this.lblAddressType.Size = new System.Drawing.Size(51, 20);
            this.lblAddressType.TabIndex = 4;
            this.lblAddressType.Text = "label1";
            // 
            // frmAddresses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1172, 592);
            this.Controls.Add(this.lblAddressType);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.txtBoxAddrFilter);
            this.Controls.Add(this.btnNewAddress);
            this.Controls.Add(this.dvgAddresses);
            this.Name = "frmAddresses";
            this.Text = "Addresses";
            this.Load += new System.EventHandler(this.Addresses_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dvgAddresses)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dvgAddresses;
        private System.Windows.Forms.Button btnNewAddress;
        private System.Windows.Forms.TextBox txtBoxAddrFilter;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Label lblAddressType;
    }
}