﻿namespace Customers
{
    partial class frmAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAddressType = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxAddr_1 = new System.Windows.Forms.TextBox();
            this.txtBoxAddr_2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbBoxCountry = new System.Windows.Forms.ComboBox();
            this.cmbBoxState = new System.Windows.Forms.ComboBox();
            this.cmbBoxCity = new System.Windows.Forms.ComboBox();
            this.txtBoxZIP = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnCountry = new System.Windows.Forms.Button();
            this.btnState = new System.Windows.Forms.Button();
            this.btnCity = new System.Windows.Forms.Button();
            this.txtBoxID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblAddressType
            // 
            this.lblAddressType.AutoSize = true;
            this.lblAddressType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddressType.Location = new System.Drawing.Point(95, 22);
            this.lblAddressType.Name = "lblAddressType";
            this.lblAddressType.Size = new System.Drawing.Size(66, 24);
            this.lblAddressType.TabIndex = 0;
            this.lblAddressType.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Addr 1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(37, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Addr 2";
            // 
            // txtBoxAddr_1
            // 
            this.txtBoxAddr_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxAddr_1.Location = new System.Drawing.Point(99, 61);
            this.txtBoxAddr_1.Name = "txtBoxAddr_1";
            this.txtBoxAddr_1.Size = new System.Drawing.Size(791, 26);
            this.txtBoxAddr_1.TabIndex = 0;
            // 
            // txtBoxAddr_2
            // 
            this.txtBoxAddr_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxAddr_2.Location = new System.Drawing.Point(99, 99);
            this.txtBoxAddr_2.Name = "txtBoxAddr_2";
            this.txtBoxAddr_2.Size = new System.Drawing.Size(791, 26);
            this.txtBoxAddr_2.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(728, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Zip";
            // 
            // cmbBoxCountry
            // 
            this.cmbBoxCountry.BackColor = System.Drawing.SystemColors.MenuBar;
            this.cmbBoxCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBoxCountry.FormattingEnabled = true;
            this.cmbBoxCountry.Location = new System.Drawing.Point(99, 139);
            this.cmbBoxCountry.Name = "cmbBoxCountry";
            this.cmbBoxCountry.Size = new System.Drawing.Size(176, 28);
            this.cmbBoxCountry.TabIndex = 3;
            this.cmbBoxCountry.SelectedIndexChanged += new System.EventHandler(this.cmbBoxCountry_SelectedIndexChanged);
            // 
            // cmbBoxState
            // 
            this.cmbBoxState.BackColor = System.Drawing.SystemColors.MenuBar;
            this.cmbBoxState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBoxState.FormattingEnabled = true;
            this.cmbBoxState.Location = new System.Drawing.Point(352, 139);
            this.cmbBoxState.Name = "cmbBoxState";
            this.cmbBoxState.Size = new System.Drawing.Size(101, 28);
            this.cmbBoxState.TabIndex = 4;
            this.cmbBoxState.SelectedIndexChanged += new System.EventHandler(this.cmbBoxState_SelectedIndexChanged);
            // 
            // cmbBoxCity
            // 
            this.cmbBoxCity.BackColor = System.Drawing.SystemColors.MenuBar;
            this.cmbBoxCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBoxCity.FormattingEnabled = true;
            this.cmbBoxCity.Location = new System.Drawing.Point(525, 139);
            this.cmbBoxCity.Name = "cmbBoxCity";
            this.cmbBoxCity.Size = new System.Drawing.Size(174, 28);
            this.cmbBoxCity.TabIndex = 5;
            // 
            // txtBoxZIP
            // 
            this.txtBoxZIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxZIP.Location = new System.Drawing.Point(765, 139);
            this.txtBoxZIP.Name = "txtBoxZIP";
            this.txtBoxZIP.Size = new System.Drawing.Size(125, 26);
            this.txtBoxZIP.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(659, 195);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 37);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(790, 195);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 37);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnCountry
            // 
            this.btnCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCountry.Location = new System.Drawing.Point(12, 139);
            this.btnCountry.Name = "btnCountry";
            this.btnCountry.Size = new System.Drawing.Size(81, 28);
            this.btnCountry.TabIndex = 16;
            this.btnCountry.Text = "Country";
            this.btnCountry.UseVisualStyleBackColor = true;
            // 
            // btnState
            // 
            this.btnState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnState.Location = new System.Drawing.Point(288, 139);
            this.btnState.Name = "btnState";
            this.btnState.Size = new System.Drawing.Size(59, 28);
            this.btnState.TabIndex = 17;
            this.btnState.Text = "State";
            this.btnState.UseVisualStyleBackColor = true;
            // 
            // btnCity
            // 
            this.btnCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCity.Location = new System.Drawing.Point(469, 139);
            this.btnCity.Name = "btnCity";
            this.btnCity.Size = new System.Drawing.Size(51, 28);
            this.btnCity.TabIndex = 18;
            this.btnCity.Text = "City";
            this.btnCity.UseVisualStyleBackColor = true;
            // 
            // txtBoxID
            // 
            this.txtBoxID.Location = new System.Drawing.Point(790, 22);
            this.txtBoxID.Name = "txtBoxID";
            this.txtBoxID.Size = new System.Drawing.Size(100, 20);
            this.txtBoxID.TabIndex = 19;
            // 
            // frmAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 256);
            this.Controls.Add(this.txtBoxID);
            this.Controls.Add(this.btnCity);
            this.Controls.Add(this.btnState);
            this.Controls.Add(this.btnCountry);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtBoxZIP);
            this.Controls.Add(this.cmbBoxCity);
            this.Controls.Add(this.cmbBoxState);
            this.Controls.Add(this.cmbBoxCountry);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBoxAddr_2);
            this.Controls.Add(this.txtBoxAddr_1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblAddressType);
            this.Name = "frmAddress";
            this.Text = "Address";
            this.Load += new System.EventHandler(this.frmAddress_Load);
            this.Shown += new System.EventHandler(this.frmAddress_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAddressType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxAddr_1;
        private System.Windows.Forms.TextBox txtBoxAddr_2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbBoxCountry;
        private System.Windows.Forms.ComboBox cmbBoxState;
        private System.Windows.Forms.ComboBox cmbBoxCity;
        private System.Windows.Forms.TextBox txtBoxZIP;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnCountry;
        private System.Windows.Forms.Button btnState;
        private System.Windows.Forms.Button btnCity;
        private System.Windows.Forms.TextBox txtBoxID;
    }
}