﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;

namespace Customers
{
    [Serializable]
    public class State
    {
        private int id = int.MinValue;
        private int id_mdcountry = int.MinValue;
        private string statecode = String.Empty;
        private string statename = String.Empty;
        
        [JsonProperty("ID")]
        public int ID { get => id; set => id = value; }
        [JsonProperty("ID_mdCountry")]
        public int ID_mdCountry { get => id_mdcountry; set => id_mdcountry = value; }
        [JsonProperty("StateCode")]
        public string StateCode { get => statecode; set => statecode = value; }
        [JsonProperty("StateName")]
        public string StateName { get => statename; set => statename = value; }

        public static string GetStateCodeFromID(int id)
        {
            string result = DBData.States.ContainsKey(id) ?
                (from s in DBData.States
                 where s.Key == id
                 select s.Value.StateCode).First() : String.Empty;
            return result;
        }
        public static string GetStateFromID(int id)
        {
            string result = DBData.States.ContainsKey(id) ?
                (from s in DBData.States
                 where s.Key == id
                 select s.Value.StateName).First() : String.Empty;
            return result;
        }
        public static List<State> GetStatesFromCountryID(int id)
        {
            var r = (from c in DBData.States
                     where c.Value.ID_mdCountry == id
                     select c.Value).ToList();
            return r;
        }
    }
}
