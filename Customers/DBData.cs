﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;

namespace Customers
{
    public class DBData
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly string GATHER_MASTERDATA_URL = @"http://www.insulationcontractorsystem.com/phpScripts/GatherMasterData.php";
        private static readonly string GATHER_ADDRESS_URL = @"http://www.insulationcontractorsystem.com/phpScripts/GatherAddresses.php";
        private static readonly string GATHER_CUSTOMERS_URL = @"http://www.insulationcontractorsystem.com/phpScripts/GatherCustomers.php";
        private static readonly string POST_DATA_URL = @"http://www.insulationcontractorsystem.com/phpScripts/PostDataToDB.php";

        public static Dictionary<int, Country> Countries = new Dictionary<int, Country>();
        public static Dictionary<int, State> States = new Dictionary<int, State>();
        public static Dictionary<int, City> Cities = new Dictionary<int, City>();
        public static Dictionary<int, Address> Addresses = new Dictionary<int, Address>();
        public static Dictionary<int, Customer> Customers = new Dictionary<int, Customer>();

        public static int result = int.MinValue;
        public static bool added = false;
        public static string dbType = String.Empty;

        public static void GatherCountries()
        {
            Task check = getCountries();
            check.Wait();
        }
        public static void GatherStates()
        {
            Task check = getStates();
            check.Wait();
        }
        public static void GatherCities()
        {
            Task check = getCities();
            check.Wait();
        }
        public static void GatherAddresses()
        {
            Task check = getAddresses();
            check.Wait();
        }
        public static void GatherCustomers()
        {
            Task check = getCustomers();
            check.Wait();
        }
        public static int PostAddressToDB(Address adr, bool inEdit)
        {
            if (inEdit == true)
                dbType = "U";
            else
                dbType = "A";

            result = int.MinValue;
            Task check = postAddress(adr);
            check.Wait();
            return result;
        }
        public static bool PostCustomerToDB(Customer cust, bool inEdit)
        {
            if (inEdit == true)
                dbType = "U";
            else
                dbType = "A";

            added = false;
            Task add = postCustomer(cust);
            add.Wait();
            return added;
        }
        private static async Task getCountries()
        {
            try
            {
                var values = new Dictionary<string, string>
                {
                    { "type", "country" }
                };
                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync(GATHER_MASTERDATA_URL, content).ConfigureAwait(continueOnCapturedContext: false);
                var responseString = await response.Content.ReadAsStringAsync();

                var cntrs = JsonConvert.DeserializeObject<List<Country>>(responseString);
                foreach(Country c in cntrs)
                {
                    Countries.Add(c.ID, c);
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }
        private static async Task getStates()
        {
            try
            {
                var values = new Dictionary<string, string>
                {
                    { "type", "state" }
                };
                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync(GATHER_MASTERDATA_URL, content).ConfigureAwait(continueOnCapturedContext: false);
                var responseString = await response.Content.ReadAsStringAsync();

                var cntrs = JsonConvert.DeserializeObject<List<State>>(responseString);
                foreach (State c in cntrs)
                {
                    States.Add(c.ID, c);
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }
        private static async Task getCities()
        {
            try
            {
                var values = new Dictionary<string, string>
                {
                    { "type", "city" }
                };
                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync(GATHER_MASTERDATA_URL, content).ConfigureAwait(continueOnCapturedContext: false);
                var responseString = await response.Content.ReadAsStringAsync();

                var cntrs = JsonConvert.DeserializeObject<List<City>>(responseString);
                foreach (City c in cntrs)
                {
                    Cities.Add(c.ID, c);
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }
        private static async Task getAddresses()
        {
            try
            {
                var values = new Dictionary<string, string>
                {
                    { "type", "address" }
                };
                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync(GATHER_ADDRESS_URL, content).ConfigureAwait(continueOnCapturedContext: false);
                var responseString = await response.Content.ReadAsStringAsync();

                var cntrs = JsonConvert.DeserializeObject<List<Address>>(responseString);
                foreach (Address c in cntrs)
                {
                    Addresses.Add(c.ID, c);
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }
        private static async Task getCustomers()
        {
            try
            {
                var values = new Dictionary<string, string>
                {
                    { "type", "customer" }
                };
                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync(GATHER_CUSTOMERS_URL, content).ConfigureAwait(continueOnCapturedContext: false);
                var responseString = await response.Content.ReadAsStringAsync();

                var cntrs = JsonConvert.DeserializeObject<List<Customer>>(responseString);
                foreach (Customer c in cntrs)
                {
                    Customers.Add(c.CompanyNumber, c);
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }
        private static async Task postAddress(Address adr)
        {
            try
            {
                var values = new Dictionary<string, string>
                {
                    { "postTO", "address" },
                    { "id", adr.ID.ToString() },
                    { "addr_1", adr.Addr_1 },
                    { "addr_2", adr.Addr_2 },
                    { "id_mdcity", adr.ID_mdCity.ToString() },
                    { "id_mdstate", adr.ID_mdState.ToString() },
                    { "zip", adr.ZIP },
                    { "id_mdcountry", adr.ID_mdCountry.ToString() },
                    { "dbtype", dbType }
                };
                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync(POST_DATA_URL, content).ConfigureAwait(continueOnCapturedContext: false);
                var responseString = await response.Content.ReadAsStringAsync();

                adr.ID = Convert.ToInt32(responseString);
                result = adr.ID;
                Addresses.Add(adr.ID, adr);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }
        private static async Task postCustomer(Customer cust)
        {
            try
            {
                var values = new Dictionary<string, string>
                {
                    { "postTO", "customer" },
                    { "companynumber", cust.CompanyNumber.ToString() },
                    { "legalname", cust.LegalName },
                    { "division", cust.Division },
                    { "federalid", cust.FederalID },
                    { "stateid", cust.StateID },
                    { "website", cust.WebSite },
                    { "pc_name", cust.PC_Name },
                    { "pc_title", cust.PC_Title },
                    { "pc_phone", cust.PC_Phone },
                    { "pc_email", cust.PC_Email },
                    { "bc_name", cust.BC_Name },
                    { "bc_title", cust.BC_Title },
                    { "bc_phone", cust.BC_Phone },
                    { "bc_email", cust.BC_Email },
                    { "companykeycode", cust.CompanyKeyCode },
                    { "dateinservice", cust.DateInService.ToString() },
                    { "dateoutofservice", cust.DateOutOfService.ToString() },
                    { "id_address_m", cust.ID_Address_M.ToString() },
                    { "id_address_b", cust.ID_Address_B.ToString() },
                    { "trialperioddays", cust.TrialPeriodDays.ToString() },
                    { "dbtype", dbType }
                };
                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync(POST_DATA_URL, content).ConfigureAwait(continueOnCapturedContext: false);
                var responseString = await response.Content.ReadAsStringAsync();

                added = (responseString == "1");

                //if (added)
                //    Customers.Add(cust.ID_Address_B, cust);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
        }

    }
}
