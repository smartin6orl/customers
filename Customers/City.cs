﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;

namespace Customers
{
    [Serializable]
    public class City
    {
        private int id = int.MinValue;
        private int id_mdstate = int.MinValue;
        private string cityname = String.Empty;
        private string cityabbrv = String.Empty;

        [JsonProperty("ID")]
        public int ID { get => id; set => id = value; }
        [JsonProperty("ID_mdState")]
        public int ID_mdState { get => id_mdstate; set => id_mdstate = value; }
        [JsonProperty("CityName")]
        public string CityName { get => cityname; set => cityname = value; }
        [JsonProperty("CityAbbrv")]
        public string CityAbbrv { get => cityabbrv; set => cityabbrv = value; }

        public static string GetCityFromID(int id)
        {
            string result = DBData.Cities.ContainsKey(id) ?
                (from s in DBData.Cities
                 where s.Key == id
                 select s.Value.CityName).First() : String.Empty;
            return result;
        }
        public static List<City> GetCitiesFromStateID(int id)
        {
            var r = (from c in DBData.Cities
                     where c.Value.ID_mdState == id
                     select c.Value).ToList();
            return r;
        }
    }
}
