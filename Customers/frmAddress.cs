﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Customers
{
    public partial class frmAddress : Form
    {
        private string addressType = String.Empty;
        private bool inEdit = false;
        Address edit = new Address();

        public frmAddress(string type)
        {
            InitializeComponent();
            addressType = type;
            this.Tag = new Address();
        }
        public frmAddress(string type, Address edit)
        {
            InitializeComponent();
            addressType = type;
            inEdit = true;
            this.edit = edit;
            this.Tag = new Address();
        }

        private void frmAddress_Load(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Address result = new Address();
            result.Addr_1 = txtBoxAddr_1.Text;
            result.Addr_2 = txtBoxAddr_2.Text;
            result.ZIP = txtBoxZIP.Text;
            if (cmbBoxCountry.SelectedIndex != -1)
                result.ID_mdCountry = (int)cmbBoxCountry.SelectedValue;
            if (cmbBoxState.SelectedIndex != -1)
                result.ID_mdState = (int)cmbBoxState.SelectedValue;
            if (cmbBoxCity.SelectedIndex != -1)
                result.ID_mdCity = (int)cmbBoxCity.SelectedValue;

            this.Tag = result;
            this.Close();
        }

        private void frmAddress_Shown(object sender, EventArgs e)
        {
            lblAddressType.Text = String.Format("Address Type: {0}", addressType);
            loadCountriesToCmbBox();
            if (inEdit)
                displayAddress(edit);
        }
        private void loadCountriesToCmbBox()
        {
            List<Country> countries = Country.GetAllCountries();
            if (countries.Count > 0)
            {
                cmbBoxCountry.ValueMember = "ID";
                cmbBoxCountry.DisplayMember = "CountryName";
                cmbBoxCountry.DataSource = new BindingSource(countries, null);
            }
            else
            {
                cmbBoxCountry.DataSource = new BindingSource(null, null);
            }
        }

        private void cmbBoxCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = (int)cmbBoxCountry.SelectedValue;
            List<State> states = State.GetStatesFromCountryID(id);
            if (states.Count > 0)
            {
                cmbBoxState.DataSource = null;
                cmbBoxState.ValueMember = "ID";
                cmbBoxState.DisplayMember = "StateName";
                cmbBoxState.DataSource = new BindingSource(states, null);
            }
            else
            {
                cmbBoxState.DataSource = new BindingSource(null, null);
            }
            if (cmbBoxState.SelectedIndex == -1)
            {
                cmbBoxCity.SelectedIndex = -1;
            }
        }

        private void cmbBoxState_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = (int)cmbBoxState.SelectedValue;
            List<City> cities = City.GetCitiesFromStateID(id);
            if (cities.Count > 0)
            {
                cmbBoxCity.DataSource = null;
                cmbBoxCity.ValueMember = "ID";
                cmbBoxCity.DisplayMember = "CityName";
                cmbBoxCity.DataSource = new BindingSource(cities, null);
            }
            else
            {
                cmbBoxCity.DataSource = new BindingSource(null, null);
            }
        }
        private void displayAddress(Address cur)
        {
            txtBoxID.Text = cur.ID.ToString();
            txtBoxAddr_1.Text = cur.Addr_1;
            txtBoxAddr_2.Text = cur.Addr_2;
            txtBoxZIP.Text = cur.ZIP;

            string country = Country.GetCountryFromID(cur.ID_mdCountry);
            int cnID = cmbBoxCountry.FindStringExact(country);
            cmbBoxCountry.SelectedIndex = cnID;

            string state = State.GetStateFromID(cur.ID_mdState);
            int stID = cmbBoxState.FindStringExact(state);
            cmbBoxState.SelectedIndex = stID;

            string city = City.GetCityFromID(cur.ID_mdCity);
            int ctID = cmbBoxCity.FindStringExact(city);
            cmbBoxCity.SelectedIndex = ctID;
        }
    }
}
