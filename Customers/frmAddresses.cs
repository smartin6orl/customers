﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Customers
{
    public partial class frmAddresses : Form
    {
        public frmAddresses(string type)
        {
            InitializeComponent();

            lblAddressType.Text = type;
        }

        private void Addresses_Load(object sender, EventArgs e)
        {
            createGrid();
            addRows();
        }
        private void createGrid()
        {
            dvgAddresses.ColumnCount = 7;

            dvgAddresses.Columns[0].Name = "ID";
            dvgAddresses.Columns[0].Visible = false;
            dvgAddresses.Columns[0].Width = 0;

            dvgAddresses.Columns[1].Name = "Street Address";
            dvgAddresses.Columns[1].Visible = true;
            dvgAddresses.Columns[1].Width = 250;

            dvgAddresses.Columns[2].Name = "City";
            dvgAddresses.Columns[2].Visible = true;
            dvgAddresses.Columns[2].Width = 150;

            dvgAddresses.Columns[3].Name = "State";
            dvgAddresses.Columns[3].Visible = true;
            dvgAddresses.Columns[3].Width = 75;

            dvgAddresses.Columns[4].Name = "Zip";
            dvgAddresses.Columns[4].Visible = true;
            dvgAddresses.Columns[4].Width = 75;

            dvgAddresses.Columns[5].Name = "Country";
            dvgAddresses.Columns[5].Visible = true;
            dvgAddresses.Columns[5].Width = 150;

            dvgAddresses.Columns[6].Name = "addressfilter";
            dvgAddresses.Columns[6].Visible = false;
            dvgAddresses.Columns[6].Width = 0;

            dvgAddresses.AllowDrop = false;
            dvgAddresses.AllowUserToAddRows = false;
            dvgAddresses.AllowUserToDeleteRows = false;
            dvgAddresses.ReadOnly = true;
        }
        private void addRows()
        {
            foreach (Address a in DBData.Addresses.Values)
            {
                addAddressToGrid(a);

                if (txtBoxAddrFilter.Text != String.Empty)
                    btnFilter.Text = "Clear Filter";
                else
                    btnFilter.Text = "Filter";
            }
        }
        private void addAddressToGrid(Address adr)
        {
            string city = City.GetCityFromID(adr.ID_mdCity);
            string state = State.GetStateFromID(adr.ID_mdState);
            string country = Country.GetCountryFromID(adr.ID_mdCountry);

            string addrFilter = (adr.Addr_1 + "|" + city + "|" + state + "|" + adr.ZIP).ToLower();
            if (txtBoxAddrFilter.Text != String.Empty)
            {

                if (addrFilter.Contains(txtBoxAddrFilter.Text.ToLower()))
                {
                    dvgAddresses.Rows.Add(adr.ID, adr.Addr_1, city, state, adr.ZIP, country, addrFilter);
                }
            }
            else
            {
                dvgAddresses.Rows.Add(adr.ID, adr.Addr_1, city, state, adr.ZIP, country, addrFilter);
            }
        }

        private void dvgAddresses_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int id = (int)dvgAddresses.SelectedCells[0].Value;
            this.Tag = id;
            this.Close();
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            dvgAddresses.Rows.Clear();

            if (btnFilter.Text == "Clear Filter")
                txtBoxAddrFilter.Text = String.Empty;

            addRows();
        }

        private void btnNewAddress_Click(object sender, EventArgs e)
        {

        }
    }
}
