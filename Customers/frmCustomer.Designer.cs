﻿namespace Customers
{
    partial class frmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtBoxCompanyNumber = new System.Windows.Forms.TextBox();
            this.txtBoxLegalName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBoxDivision = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxFederalID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBoxStateID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBoxWebSite = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBoxPC_Name = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBoxPC_Title = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBoxPC_Email = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBoxPC_Phone = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBoxBC_Phone = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBoxBC_Email = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBoxBC_Title = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBoxBC_Name = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtBoxAddressB = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtBoxAddressM = new System.Windows.Forms.TextBox();
            this.btnMail = new System.Windows.Forms.Button();
            this.btnBill = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtBoxTrialDays = new System.Windows.Forms.TextBox();
            this.btnGenerateKeyCode = new System.Windows.Forms.Button();
            this.txtBoxCompanyKeyCode = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtBoxDateInService = new System.Windows.Forms.TextBox();
            this.txtBoxDateOutOfService = new System.Windows.Forms.TextBox();
            this.chkBoxUsePrimary = new System.Windows.Forms.CheckBox();
            this.txtBoxMail_ID = new System.Windows.Forms.TextBox();
            this.txtBoxBill_ID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Company Number";
            // 
            // txtBoxCompanyNumber
            // 
            this.txtBoxCompanyNumber.Location = new System.Drawing.Point(170, 67);
            this.txtBoxCompanyNumber.Name = "txtBoxCompanyNumber";
            this.txtBoxCompanyNumber.Size = new System.Drawing.Size(161, 20);
            this.txtBoxCompanyNumber.TabIndex = 1;
            // 
            // txtBoxLegalName
            // 
            this.txtBoxLegalName.Location = new System.Drawing.Point(170, 109);
            this.txtBoxLegalName.Name = "txtBoxLegalName";
            this.txtBoxLegalName.Size = new System.Drawing.Size(816, 20);
            this.txtBoxLegalName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Legal Name";
            // 
            // txtBoxDivision
            // 
            this.txtBoxDivision.Location = new System.Drawing.Point(170, 136);
            this.txtBoxDivision.Name = "txtBoxDivision";
            this.txtBoxDivision.Size = new System.Drawing.Size(816, 20);
            this.txtBoxDivision.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Division";
            // 
            // txtBoxFederalID
            // 
            this.txtBoxFederalID.Location = new System.Drawing.Point(170, 159);
            this.txtBoxFederalID.Name = "txtBoxFederalID";
            this.txtBoxFederalID.Size = new System.Drawing.Size(161, 20);
            this.txtBoxFederalID.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Federal ID";
            // 
            // txtBoxStateID
            // 
            this.txtBoxStateID.Location = new System.Drawing.Point(485, 159);
            this.txtBoxStateID.Name = "txtBoxStateID";
            this.txtBoxStateID.Size = new System.Drawing.Size(161, 20);
            this.txtBoxStateID.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(418, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "State ID";
            // 
            // txtBoxWebSite
            // 
            this.txtBoxWebSite.Location = new System.Drawing.Point(170, 185);
            this.txtBoxWebSite.Name = "txtBoxWebSite";
            this.txtBoxWebSite.Size = new System.Drawing.Size(816, 20);
            this.txtBoxWebSite.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(54, 188);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Web Site";
            // 
            // txtBoxPC_Name
            // 
            this.txtBoxPC_Name.Location = new System.Drawing.Point(170, 237);
            this.txtBoxPC_Name.Name = "txtBoxPC_Name";
            this.txtBoxPC_Name.Size = new System.Drawing.Size(551, 20);
            this.txtBoxPC_Name.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(86, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Name (F L)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(43, 217);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 16);
            this.label8.TabIndex = 14;
            this.label8.Text = "Primary Contact";
            // 
            // txtBoxPC_Title
            // 
            this.txtBoxPC_Title.Location = new System.Drawing.Point(170, 262);
            this.txtBoxPC_Title.Name = "txtBoxPC_Title";
            this.txtBoxPC_Title.Size = new System.Drawing.Size(551, 20);
            this.txtBoxPC_Title.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(118, 265);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Title";
            // 
            // txtBoxPC_Email
            // 
            this.txtBoxPC_Email.Location = new System.Drawing.Point(170, 286);
            this.txtBoxPC_Email.Name = "txtBoxPC_Email";
            this.txtBoxPC_Email.Size = new System.Drawing.Size(551, 20);
            this.txtBoxPC_Email.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(72, 289);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Email Address";
            // 
            // txtBoxPC_Phone
            // 
            this.txtBoxPC_Phone.Location = new System.Drawing.Point(170, 309);
            this.txtBoxPC_Phone.Name = "txtBoxPC_Phone";
            this.txtBoxPC_Phone.Size = new System.Drawing.Size(220, 20);
            this.txtBoxPC_Phone.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(67, 312);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Phone Number";
            // 
            // txtBoxBC_Phone
            // 
            this.txtBoxBC_Phone.Location = new System.Drawing.Point(170, 431);
            this.txtBoxBC_Phone.Name = "txtBoxBC_Phone";
            this.txtBoxBC_Phone.Size = new System.Drawing.Size(220, 20);
            this.txtBoxBC_Phone.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(67, 434);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Phone Number";
            // 
            // txtBoxBC_Email
            // 
            this.txtBoxBC_Email.Location = new System.Drawing.Point(170, 408);
            this.txtBoxBC_Email.Name = "txtBoxBC_Email";
            this.txtBoxBC_Email.Size = new System.Drawing.Size(551, 20);
            this.txtBoxBC_Email.TabIndex = 27;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(72, 411);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Email Address";
            // 
            // txtBoxBC_Title
            // 
            this.txtBoxBC_Title.Location = new System.Drawing.Point(170, 384);
            this.txtBoxBC_Title.Name = "txtBoxBC_Title";
            this.txtBoxBC_Title.Size = new System.Drawing.Size(551, 20);
            this.txtBoxBC_Title.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(118, 387);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Title";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(43, 339);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(107, 16);
            this.label15.TabIndex = 23;
            this.label15.Text = "Billing Contact";
            // 
            // txtBoxBC_Name
            // 
            this.txtBoxBC_Name.Location = new System.Drawing.Point(170, 359);
            this.txtBoxBC_Name.Name = "txtBoxBC_Name";
            this.txtBoxBC_Name.Size = new System.Drawing.Size(551, 20);
            this.txtBoxBC_Name.TabIndex = 22;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(86, 362);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 13);
            this.label16.TabIndex = 21;
            this.label16.Text = "Name (F L)";
            // 
            // txtBoxAddressB
            // 
            this.txtBoxAddressB.BackColor = System.Drawing.SystemColors.Menu;
            this.txtBoxAddressB.Enabled = false;
            this.txtBoxAddressB.Location = new System.Drawing.Point(170, 515);
            this.txtBoxAddressB.Name = "txtBoxAddressB";
            this.txtBoxAddressB.Size = new System.Drawing.Size(816, 20);
            this.txtBoxAddressB.TabIndex = 34;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(62, 467);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 16);
            this.label20.TabIndex = 32;
            this.label20.Text = "Addresses";
            // 
            // txtBoxAddressM
            // 
            this.txtBoxAddressM.BackColor = System.Drawing.SystemColors.Menu;
            this.txtBoxAddressM.Enabled = false;
            this.txtBoxAddressM.Location = new System.Drawing.Point(170, 489);
            this.txtBoxAddressM.Name = "txtBoxAddressM";
            this.txtBoxAddressM.Size = new System.Drawing.Size(816, 20);
            this.txtBoxAddressM.TabIndex = 31;
            // 
            // btnMail
            // 
            this.btnMail.Location = new System.Drawing.Point(85, 487);
            this.btnMail.Name = "btnMail";
            this.btnMail.Size = new System.Drawing.Size(75, 23);
            this.btnMail.TabIndex = 35;
            this.btnMail.Text = "Mailing";
            this.btnMail.UseVisualStyleBackColor = true;
            this.btnMail.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnBill
            // 
            this.btnBill.Location = new System.Drawing.Point(85, 513);
            this.btnBill.Name = "btnBill";
            this.btnBill.Size = new System.Drawing.Size(75, 23);
            this.btnBill.TabIndex = 36;
            this.btnBill.Text = "Billing";
            this.btnBill.UseVisualStyleBackColor = true;
            this.btnBill.Click += new System.EventHandler(this.btnBill_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(167, 570);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 13);
            this.label17.TabIndex = 37;
            this.label17.Text = "Date In Service";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(680, 570);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(101, 13);
            this.label18.TabIndex = 38;
            this.label18.Text = "Date Out of Service";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(482, 570);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 13);
            this.label19.TabIndex = 41;
            this.label19.Text = "Trial Prd Days";
            // 
            // txtBoxTrialDays
            // 
            this.txtBoxTrialDays.Location = new System.Drawing.Point(561, 567);
            this.txtBoxTrialDays.Name = "txtBoxTrialDays";
            this.txtBoxTrialDays.Size = new System.Drawing.Size(85, 20);
            this.txtBoxTrialDays.TabIndex = 40;
            // 
            // btnGenerateKeyCode
            // 
            this.btnGenerateKeyCode.Location = new System.Drawing.Point(46, 623);
            this.btnGenerateKeyCode.Name = "btnGenerateKeyCode";
            this.btnGenerateKeyCode.Size = new System.Drawing.Size(114, 23);
            this.btnGenerateKeyCode.TabIndex = 43;
            this.btnGenerateKeyCode.Text = "Generate Key Code";
            this.btnGenerateKeyCode.UseVisualStyleBackColor = true;
            this.btnGenerateKeyCode.Click += new System.EventHandler(this.btnGenerateKeyCode_Click);
            // 
            // txtBoxCompanyKeyCode
            // 
            this.txtBoxCompanyKeyCode.BackColor = System.Drawing.SystemColors.Menu;
            this.txtBoxCompanyKeyCode.Enabled = false;
            this.txtBoxCompanyKeyCode.Location = new System.Drawing.Point(170, 625);
            this.txtBoxCompanyKeyCode.Name = "txtBoxCompanyKeyCode";
            this.txtBoxCompanyKeyCode.Size = new System.Drawing.Size(816, 20);
            this.txtBoxCompanyKeyCode.TabIndex = 44;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(720, 678);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(109, 36);
            this.btnSave.TabIndex = 45;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(877, 678);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(109, 36);
            this.btnCancel.TabIndex = 46;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtBoxDateInService
            // 
            this.txtBoxDateInService.Location = new System.Drawing.Point(254, 567);
            this.txtBoxDateInService.Name = "txtBoxDateInService";
            this.txtBoxDateInService.Size = new System.Drawing.Size(179, 20);
            this.txtBoxDateInService.TabIndex = 47;
            // 
            // txtBoxDateOutOfService
            // 
            this.txtBoxDateOutOfService.Location = new System.Drawing.Point(787, 567);
            this.txtBoxDateOutOfService.Name = "txtBoxDateOutOfService";
            this.txtBoxDateOutOfService.Size = new System.Drawing.Size(167, 20);
            this.txtBoxDateOutOfService.TabIndex = 48;
            // 
            // chkBoxUsePrimary
            // 
            this.chkBoxUsePrimary.AutoSize = true;
            this.chkBoxUsePrimary.Location = new System.Drawing.Point(170, 337);
            this.chkBoxUsePrimary.Name = "chkBoxUsePrimary";
            this.chkBoxUsePrimary.Size = new System.Drawing.Size(82, 17);
            this.chkBoxUsePrimary.TabIndex = 49;
            this.chkBoxUsePrimary.Text = "Use Primary";
            this.chkBoxUsePrimary.UseVisualStyleBackColor = true;
            this.chkBoxUsePrimary.CheckedChanged += new System.EventHandler(this.chkBoxUsePrimary_CheckedChanged);
            // 
            // txtBoxMail_ID
            // 
            this.txtBoxMail_ID.Location = new System.Drawing.Point(12, 490);
            this.txtBoxMail_ID.Name = "txtBoxMail_ID";
            this.txtBoxMail_ID.Size = new System.Drawing.Size(72, 20);
            this.txtBoxMail_ID.TabIndex = 50;
            // 
            // txtBoxBill_ID
            // 
            this.txtBoxBill_ID.Location = new System.Drawing.Point(12, 515);
            this.txtBoxBill_ID.Name = "txtBoxBill_ID";
            this.txtBoxBill_ID.Size = new System.Drawing.Size(72, 20);
            this.txtBoxBill_ID.TabIndex = 51;
            // 
            // frmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 755);
            this.Controls.Add(this.txtBoxBill_ID);
            this.Controls.Add(this.txtBoxMail_ID);
            this.Controls.Add(this.chkBoxUsePrimary);
            this.Controls.Add(this.txtBoxDateOutOfService);
            this.Controls.Add(this.txtBoxDateInService);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtBoxCompanyKeyCode);
            this.Controls.Add(this.btnGenerateKeyCode);
            this.Controls.Add(this.txtBoxTrialDays);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.btnBill);
            this.Controls.Add(this.btnMail);
            this.Controls.Add(this.txtBoxAddressB);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtBoxAddressM);
            this.Controls.Add(this.txtBoxBC_Phone);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtBoxBC_Email);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtBoxBC_Title);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtBoxBC_Name);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtBoxPC_Phone);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtBoxPC_Email);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtBoxPC_Title);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtBoxPC_Name);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBoxWebSite);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtBoxStateID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtBoxFederalID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBoxDivision);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBoxLegalName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBoxCompanyNumber);
            this.Controls.Add(this.label1);
            this.Name = "frmCustomer";
            this.Text = "Customer";
            this.Load += new System.EventHandler(this.frmCustomer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBoxCompanyNumber;
        private System.Windows.Forms.TextBox txtBoxLegalName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBoxDivision;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxFederalID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBoxStateID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBoxWebSite;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBoxPC_Name;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBoxPC_Title;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBoxPC_Email;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBoxPC_Phone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBoxBC_Phone;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtBoxBC_Email;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBoxBC_Title;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtBoxBC_Name;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtBoxAddressB;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtBoxAddressM;
        private System.Windows.Forms.Button btnMail;
        private System.Windows.Forms.Button btnBill;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtBoxTrialDays;
        private System.Windows.Forms.Button btnGenerateKeyCode;
        private System.Windows.Forms.TextBox txtBoxCompanyKeyCode;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtBoxDateInService;
        private System.Windows.Forms.TextBox txtBoxDateOutOfService;
        private System.Windows.Forms.CheckBox chkBoxUsePrimary;
        private System.Windows.Forms.TextBox txtBoxMail_ID;
        private System.Windows.Forms.TextBox txtBoxBill_ID;
    }
}