﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace Customers
{
    public partial class frmCustomers : Form
    {
        public frmCustomers()
        {
            InitializeComponent();
        }
        private void frmCustomers_Load(object sender, EventArgs e)
        {
            //Gather all the Master Data from the Database
            DBData.GatherCountries();
            DBData.GatherStates();
            DBData.GatherCities();
            DBData.GatherAddresses();
            DBData.GatherCustomers();

            createGrid();
            addRows();
        }
        private void createGrid()
        {
            dgvCustomers.ColumnCount = 5;
            dgvCustomers.Columns[0].Name = "Company Number";
            dgvCustomers.Columns[0].Visible = true;
            dgvCustomers.Columns[0].Width = 150;

            dgvCustomers.Columns[1].Name = "Legal Name";
            dgvCustomers.Columns[1].Visible = true;
            dgvCustomers.Columns[1].Width = 240;

            dgvCustomers.Columns[2].Name = "Division";
            dgvCustomers.Columns[2].Visible = true;
            dgvCustomers.Columns[2].Width = 240;

            dgvCustomers.Columns[3].Name = "Date In Service";
            dgvCustomers.Columns[3].Visible = true;
            dgvCustomers.Columns[3].Width = 100;

            dgvCustomers.Columns[4].Name = "Date Out of Service";
            dgvCustomers.Columns[4].Visible = true;
            dgvCustomers.Columns[4].Width = 100;
        }
        private void btnNewCustomer_Click(object sender, EventArgs e)
        {
            frmCustomer cust = new frmCustomer();
            cust.ShowDialog();
        }
        private void addRows()
        {
            foreach (Customer c in DBData.Customers.Values)
            {
                addCustomerToGrid(c);
            }
        }
        private void addCustomerToGrid(Customer cust)
        {
            string dateInService = (cust.DateInService != DateTime.MinValue) ?
                cust.DateInService.ToString() : String.Empty;
            string dateOutOfService = (cust.DateOutOfService != DateTime.MinValue) ?
                cust.DateOutOfService.ToString() : String.Empty;
            dgvCustomers.Rows.Add(cust.CompanyNumber, cust.LegalName, cust.Division, dateInService, dateOutOfService);
        }
        private void dgvCustomers_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int id = (int)dgvCustomers.SelectedCells[0].Value;
            Customer selected = Customer.GetCustomerFromID(id);
            frmCustomer editCust = new frmCustomer(selected);
            editCust.ShowDialog();
        }
    }
}
