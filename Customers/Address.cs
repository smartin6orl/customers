﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;

namespace Customers
{
    [Serializable]
    public class Address
    {
        public static readonly string ADDR_TYPE_BILLING = "Billing";
        public static readonly string ADDR_TYPE_MAILING = "Mailing";

        private int id = int.MinValue;
        private string addr_1 = String.Empty;
        private string addr_2 = String.Empty;
        private int id_mdcity = int.MinValue;
        private int id_mdstate = int.MinValue;
        private string zip = String.Empty;
        private int id_mdcountry = int.MinValue;

        [JsonProperty("ID")]
        public int ID { get => id; set => id = value; }
        [JsonProperty("Addr_1")]
        public string Addr_1 { get => addr_1; set => addr_1 = value; }
        [JsonProperty("Addr_2")]
        public string Addr_2 { get => addr_2; set => addr_2 = value; }
        [JsonProperty("ID_mdCity")]
        public int ID_mdCity { get => id_mdcity; set => id_mdcity = value; }
        [JsonProperty("ID_mdState")]
        public int ID_mdState { get => id_mdstate; set => id_mdstate = value; }
        [JsonProperty("ZIP")]
        public string ZIP { get => zip; set => zip = value; }
        [JsonProperty("ID_mdCountry")]
        public int ID_mdCountry { get => id_mdcountry; set => id_mdcountry = value; }

        public static Address GetAddressFromID(int id)
        {
            Address adr = DBData.Addresses.ContainsKey(id) ?
                (from a in DBData.Addresses
                 where a.Key == id
                 select a.Value).First() : new Address();
            return adr;
        }
        public override string ToString()
        {
            string city = City.GetCityFromID(this.ID_mdCity);
            string stAbbr = (this.ID_mdState != int.MinValue) ?
                String.Format("{0}", State.GetStateCodeFromID(this.ID_mdState)) : String.Empty;
            string country = Country.GetCountryAbbrFromID(this.ID_mdCountry);
            return String.Format("{0} {1} {2} {3} {4} {5}", this.Addr_1, Addr_2, city, stAbbr, this.ZIP, country);
        }
    }
}
