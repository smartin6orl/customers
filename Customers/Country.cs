﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Customers
{
    [Serializable]
    public class Country
    {
        private int id = int.MinValue;
        private string countryname = String.Empty;
        private string code = String.Empty;

        [JsonProperty("ID")]
        public int ID { get => id; set => id = value; }
        [JsonProperty("Country")]
        public string CountryName { get => countryname; set => countryname = value; }
        [JsonProperty("Code")]
        public string Code { get => code; set => code = value; }

        public static string GetCountryAbbrFromID(int id)
        {
            string result = DBData.Countries.ContainsKey(id) ?
                (from s in DBData.Countries
                 where s.Key == id
                 select s.Value.Code).First() : String.Empty;
            return result;
        }
        public static string GetCountryFromID(int id)
        {
            string result = DBData.Countries.ContainsKey(id) ?
                (from s in DBData.Countries
                 where s.Key == id
                 select s.Value.CountryName).First() : String.Empty;
            return result;
        }
        public static List<Country> GetAllCountries()
        {
            return DBData.Countries.Values.ToList();
        }
    }
}
